package bitmap.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitmapDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BitmapDemoApplication.class, args);
    }

}
