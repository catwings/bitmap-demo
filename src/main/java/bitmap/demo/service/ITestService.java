package bitmap.demo.service;

import java.util.List;

public interface ITestService {

    void genTestData();

    List<Integer> getPushData();

}
