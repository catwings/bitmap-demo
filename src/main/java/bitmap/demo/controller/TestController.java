package bitmap.demo.controller;

import bitmap.demo.common.CommonResponse;
import bitmap.demo.service.ITestService;
import bitmap.demo.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@RestController
@RequestMapping("/test/")
public class TestController {

    @Autowired
    private ITestService testService;

    @GetMapping("getPushData")
    public CommonResponse getPushData(){
        List<Integer> pushData = testService.getPushData();
        return CommonResponse.SUCCESS().data(pushData);
    }

    /**
     * 生成测试数据
     * @return
     */
    @PostMapping("genTestData")
    public CommonResponse genTestData(){
        testService.genTestData();
        return CommonResponse.SUCCESS();
    }


}
