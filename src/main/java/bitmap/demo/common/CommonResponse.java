package bitmap.demo.common;

import org.springframework.http.HttpStatus;

import java.util.HashMap;

public class CommonResponse extends HashMap<String, Object> {

    private static final long serialVersionUID = -8713837118340960775L;

    private CommonResponse(){super();}
    private CommonResponse(HttpStatus status){
        this.put("code", status.value());
    }

    public CommonResponse status(String status) {
        this.put("status", status);
        return this;
    }

    public CommonResponse message(String message) {
        this.put("message", message);
        return this;
    }

    public CommonResponse data(Object data) {
        this.put("data", data);
        return this;
    }

    @Override
    public CommonResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }


    public static CommonResponse SUCCESS(){
        return new CommonResponse(HttpStatus.OK).status(RespStatus.success.getCode());
    }

    public static CommonResponse FAIL(){
        return new CommonResponse(HttpStatus.OK).status(RespStatus.fail.getCode());
    }

}
